from django.views.generic import DetailView
from django.shortcuts import render
'''
from core.models import News


def view1(request):
    """View that render html page with context"""
    page = 'this is default view'
    return render(request, 'core.html', {'page': page})


class View2(DetailView):
    """Class-based view that render html page with context"""
    queryset = News.objects.all()
    template_name = 'core.html'
    """Return context with adding of 'page' value"""
    def get_context_data(self, **kwargs):
        context = super(View2, self).get_context_data(**kwargs)
        context['page'] = 'this is class-based view'

        return context
'''

