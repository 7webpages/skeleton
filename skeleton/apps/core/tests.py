from __future__ import unicode_literals, absolute_import

from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

from django_webtest import WebTest


class ExampleTest(WebTest):

    def test_get_request(self):
        """Test that checks for getting request by some url"""
        response = self.app.get(reverse('view1'))
        self.assertTrue('html' in response)

    def test_form_submit(self):
        """Test that submit form and check entered field value"""
        page = self.app.get(reverse('some-view-with-form'))
        page.form['some-field'] = 'some-value'
        response = page.form.submit()
        self.assertEqual(response.context['some-field'], 'some-value')

    def test_rendered_templates(self):
        """Test that check response context"""
        response = self.app.get('/template/index.html')
        self.assertTrue(hasattr(response, 'context'))
        self.assertTrue(hasattr(response, 'template'))

        self.assertEqual(response.template.name, 'index.html')
        self.assertEqual(response.context['bar'], True)
        self.assertEqual(response.context['spam'], None)
